const fs = require('fs').promises
const jikanjs = require('jikanjs');

// full update 0 or 1 for partial
const mode = 1

const checkIfSameByID = (array) => {

    const idsArray = array.map(el => el.id)
    let data = idsArray.filter((item, index) => idsArray.indexOf(item) != index)
    return new Set(data)
}

const getAnimeListReponses = (array, cb) => {
    let responses = []

    const execute = () => {
        cb(responses)
        if (!array.length) {
            return
        }
        console.log('animeFilesLength:', array.length)

        const anime = array.shift()
        console.log('Anime id:', anime.id)
        jikanjs.loadAnime(anime.id)
            .then(response => {
                setTimeout(() => {
                    responses.push(response)
                    execute()
                }, 8000)
            })
            .catch(console.log)
    }

    execute()
}

const createNewAnimeDBfromMyAnimeList = (array) => {

    const newArray = array.sort((a, b) => a.id - b.id).map(anime => {

        const genres = anime.genres.map(genre => genre.name)

        return {
            id: anime.mal_id,
            title: anime.title,
            title_english: anime.title_english,
            episodes: anime.episodes,
            airing: anime.airing,
            rank: anime.rank,
            image_url: anime.image_url,
            genres: genres
        }
    })

    return newArray

}

const start = () => {
    const animesFile = require('./animesDB.json')
    animesFile.sort((a, b) => a.id - b.id)

    const set = checkIfSameByID(animesFile)
    if (set.size !== 0) {
        console.log(`Not unique ids found: ${set}`)
        return
    }

    const animesArray = mode === 1 ? animesFile.filter(anime => !anime.title) : animesFile
    getAnimeListReponses(animesArray, (responses) => {
        const newAnimesForDB = createNewAnimeDBfromMyAnimeList(responses)

        if (mode === 1) {
            newAnimesForDB.forEach(element => {
                const foundIndex = animesFile.findIndex(anime => anime.id === element.id);
                console.log('foundIndex', foundIndex)
                if (foundIndex !== -1) {
                    animesFile[foundIndex] = element
                } else {
                    console.log(`${element.id} had some issues`)
                }
            })
        }


        fs.writeFile(`./animesDB.json`, JSON.stringify(mode === 1 ? animesFile : newAnimesForDB, null, 4))

    })
}

start()